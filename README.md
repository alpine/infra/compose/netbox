# Netbox Docker

Run Netbox in docker with docker-compose. Based on [netbox-docker][] from the
Netbox community.

[netbox-docker]:https://github.com/netbox-community/netbox-docker

## Usage

Create the environment files in `env/` from the templates and fill in the
missing values.

If you want to create a clean instance you can just run `docker-compose up -d`

When you want to use an existing DB, you can use the following recipe:

```sh
$ docker-compose up -d postgres
$ docker exec -i <container_name> psql -U netbox -d netbox <database.sql`
$ docker-compose up -d
```

After this, the instance should be up and running.
